import socket
import configparser

# Read connection parameters from config file
server_config = configparser.ConfigParser()
server_config.read("config.ini")


host = server_config.get('Server', 'host')
PORT = int(server_config.get('Server', 'port'))

# socket.AF_INET6 is used from IPv6, socket.SOCK_STREAM is for TCP
client = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
client.connect((host, PORT))

def receive():
    # print('In receive')
    while True:
        try:
            timestamp = client.recv(1024).decode('ascii')
            print(timestamp)
        except:
            print('Connection Interupted')
            client.close()
            break


receive()