import configparser
import socket
import threading
import datetime
import time
import configparser

# Read connection parameters from config file
server_config = configparser.ConfigParser()
server_config.read("config.ini")

host = server_config.get('Server', 'host')
PORT = int(server_config.get('Server', 'port'))
print('Host: {}, Port :{} '.format(host,PORT))

# socket.AF_INET6 is used from IPv6, socket.SOCK_STREAM is for TCP
server = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
server.bind((host,PORT))
server.listen()

def receive():
    while True:
        client, addr = server.accept()
        print('Client connected with address: {}'.format(str(addr)))

        thread = threading.Thread(target=entertainClient, args={client})
        thread.start()


#each client request is handled via thread
def entertainClient(client):
    while True:
        try:
            timestamp = getIsoDateTimeStamp()
            client.send(timestamp.encode('ascii'))
            print('timestamp stamp: {} sent to the client. '.format(timestamp))
            time.sleep(5)
        except Exception as e:
            print(str(e))
            client.close()
            break




def getIsoDateTimeStamp():
    return datetime.datetime.now().isoformat() + '\n'


print('Server  up & running')
receive()
